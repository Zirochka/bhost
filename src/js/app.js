import 'slick-carousel';
import _ from 'lodash';

const $headerSlider = $('.js-header-slider');
const $pricingSlider = $('.js-pricing-slider');
const $subscribeSlider = $('.js-subscribe-slider');
const $reviewSlider = $('.js-review-slider-slick');

$headerSlider.slick({
  nextArrow: '<button class="slick-arrow slick-next js-arrow"></button>',
  prevArrow: '<button class="slick-arrow slick-prev js-arrow"></button>'
});

$pricingSlider.slick({
  dots: true,
  fade: true,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        arrows: false
      }
    }
  ]
});

$subscribeSlider.slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 728,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 2
      }
    },
  ]
});

$reviewSlider.slick({
  arrows: false,
  dots: true,
  slidesToShow: 2,
  responsive: [
    {
      breakpoint: 576,
      settings: {
        slidesToShow: 1
      }
    }
  ]
});

const $headerSocial = $('.social');
const containerWidth = 1170;
const offsetContainer = 15;
const $arrow = $('.js-arrow');
const arrowWidth = $arrow.width();
const $menu = $('.menu');
const $hamburger = $('.hamburger');
const activeStatus = 'is-active';

const tabData = {
  tab: $('.js-tab'),
  tabList: $('.js-tab-list'),
  tabItem: $('.js-tab-item'),
  tabContent: $('.js-tab-content-item'),
  active: 'is-active'
}

const accordionData = {
  accordion: $('.js-accordion'),
  accordionItem: $('.js-accordion-item'),
  accordionContent: $('.js-accordion-content-item'),
  active: 'is-active'
}

const newTab = [];

const calculateOffset = () => {
  if ($(window).width() < 1200) {
    $headerSocial.offset({left: offsetContainer});
    $arrow.offset({left: $(window).width() - (arrowWidth + offsetContainer)});
  } else {
    $headerSocial.offset({left: ($(window).width() - containerWidth) / 2});
    $arrow.offset({left: ($(window).width() - ($(window).width() - containerWidth) / 2) - arrowWidth});
  }
}

calculateOffset();

$(window).on('resize', _.debounce(calculateOffset, 150));

class Tab {
  constructor(data, i) {
    this.tab = data.tab.eq(i);
    this.tabList = this.tab.find(data.tabList);
    this.tabItem = this.tab.find(data.tabItem);
    this.tabContent = this.tab.find(data.tabContent);
    this.active = data.active;
    this.handler = this.handler.bind(this);
  }

  showContent() {
    this.tabItem.eq(0).addClass(this.active);
    this.tabContent.eq(0).fadeIn();
  }

  handler(event) {
    const $currentTarget = $(event.target);
    if ($currentTarget.parent().hasClass('js-tab-item')) {
      this.tabItem.removeClass(this.active).has($currentTarget).addClass(this.active);
      this.tabContent.hide().eq(this.tabItem.index($currentTarget.parent())).fadeIn();
    }
  }

  start() {
    this.showContent();
    this.tab.on('click', this.handler);
  }
}

const tabCreate = () => {
  for (let i = 0; i < tabData.tab.length; i++) {
    newTab[i] = new Tab(tabData, i);
    newTab[i].start();
  }
}

tabCreate();

class Accordion {
  constructor(data) {
    this.accordion = data.accordion;
    this.accordionItem = this.accordion.find(data.accordionItem);
    this.accordionContent = this.accordion.find(data.accordionContent);
    this.active = data.active;
    this.handler = this.handler.bind(this);
  }

  handler(event) {
    const $currentTarget = $(event.target);
    if (this.accordion.has($currentTarget)) {
      if (this.accordionItem.has($currentTarget).hasClass(this.active)) {
        this.accordionItem.has($currentTarget).removeClass(this.active);
        this.accordionItem.has($currentTarget).find(this.accordionContent).slideUp(1000);
      } else {
        this.accordionItem.has($currentTarget).addClass(this.active);
        this.accordionItem.has($currentTarget).find(this.accordionContent).slideDown(1000);
      }
    }
  }

  start() {
    this.accordion.on('click', this.handler);
  }
}

const accordion = new Accordion(accordionData)
accordion.start();

const mobileMenu = () => {
  $hamburger.toggleClass(activeStatus);
  $menu.toggleClass(activeStatus);
}

$hamburger.on('click', mobileMenu);

class Slider {
  constructor () {
    this.slider = $('.js-review-slider');
    this.wrapper = this.slider.find($('.js-slider-wrap'));
    this.slidesHolder = this.slider.find($('.js-slider-holder'));
    this.slides = this.slider.find($('.js-slider-slide'));
    this.descriptionsHolder = this.slider.find($('.js-slider-content'));
    this.descriptions = this.slider.find($('.js-slider-content-item'));
    this.currentAngle = 0;
    this.currentSlide = 0;
    this.countSlides = this.slides.length;
    this.dotsWrap = this.slider.find($('.js-slider-dots'));
    this.activeClass = 'is-active';
    this.coordinate = {};
    this.onResize = this.onResize.bind(this);
    this.rotate = this.rotate.bind(this);
  }

  onResize() {
    let radius;
    const width = this.slider.width();
    const height = this.slider.height();

    if (2 * height <= width) {
      radius = width / 2;
    } else {
      radius = width / 2;
    }

    this.setSize( Math.round( radius ) );
  }  
  
  setSize(radius) {
    this.slidesRepositioning(0.85 * radius);

    this.slider.css('marginTop', `-${0.4 * radius }px`);
    this.wrapper.css({'width': `${2 * radius }px`, 'height': `${radius }px`, 'background-position': `center ${0.4 * radius - 12 }px`});
    this.slidesHolder.css({'width': `${1.7 * radius }px`, 'height': `${1.7 * radius }px`, 'marginTop': `${0.4 * radius }px`});
    this.descriptionsHolder.css({'width': `${1.16 * radius }px`, 'height': `${0.3 * radius }px`});
  }
  
  slidesRepositioning(r) {
    for (let i = 0; i < this.countSlides; i++) {
      const x = r * Math.cos(2 * Math.PI / this.countSlides * i - Math.PI / 2);
      const y = r * Math.sin(2 * Math.PI / this.countSlides * i - Math.PI / 2);

      this.coordinate[i] = {x, y};
      this.slides.eq(i).css('transform', `translate( ${ x  }px, ${ y }px ) rotate( ${ -this.currentAngle }deg )`);
    }
  }

  rotate(multiplier) {
    this.removeStyle();

    this.currentSlide = multiplier.data.multiplier;
    this.currentAngle = -360 / this.countSlides * multiplier.data.multiplier;
    this.slidesHolder.css('transform', `rotate( ${  this.currentAngle  }deg )`);
    for( let i = 0; i < this.countSlides; i++ ) {
      this.slides.eq(i).css('transform', `translate( ${ this.coordinate[i].x }px, ${ this.coordinate[i].y }px ) rotate( ${ -this.currentAngle }deg )`);
    }

    this.addStyle();
  }

  removeStyle() {
    this.descriptions.eq(this.currentSlide).removeClass(this.activeClass);
    this.slides.eq(this.currentSlide).removeClass(this.activeClass);
    this.dotsItem.eq(this.currentSlide).removeClass(this.activeClass)
  }

  addStyle() {
    this.descriptions.eq(this.currentSlide).addClass(this.activeClass);
    this.slides.eq(this.currentSlide).addClass(this.activeClass);
    this.dotsItem.eq(this.currentSlide).addClass(this.activeClass);
  }

  setNav() {
    for (let i = 0; i < this.countSlides; i++) {
      $('<li>', {class: 'review-slider__dots-item'}).appendTo(this.dotsWrap);
    }
    this.dotsItem = this.dotsWrap.find($('.review-slider__dots-item'));

    for (let i = 0; i < this.dotsItem.length; i++) {
      this.dotsItem.eq(i).on('click', {
        multiplier: i
      }, this.rotate);
    }
  }

  start() {
    this.onResize();
    this.setNav();
    this.addStyle();

    $(window).on('resize', this.onResize);
  }
}

const sliderCircle = new Slider();
sliderCircle.start();